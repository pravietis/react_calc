import React from "react";
import '../css/Display.css'

class ResultDisplay extends React.Component{
    render(){
        return (
            <div className="Display">
                <p>{this.props.result}</p>
            </div>
        )
    }
}

export default ResultDisplay