import React from "react";
import '../css/Button.css'

class DotButton extends React.Component{
    constructor() {
        super();
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn">
                <p>.</p>
            </div>
        )
    }

    inputClick(){
        if (this.props.state.result.indexOf(".") >= 0){
            return
        }

        let result;
        if (this.props.state.digitAppend == false) {
            console.log()
            result = "0."
        }
        else {
            result = this.props.state.result + "."
        }

        this.props.inputClick({
            result: result,
            digitAppend: true,
        })
    }
}

export default DotButton