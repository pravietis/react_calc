import React from "react";
import '../css/Button.css'

class ClearButton extends React.Component{
    constructor() {
        super();
        this.inputClick= this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn" id="functional">
                <p>C</p>
            </div>
        )
    }

    inputClick(){
        this.props.inputClick({
            result: "0",
            temp: 0,
            operator: "equal",
            digitAppend: false,
        });
    }
}

export default ClearButton