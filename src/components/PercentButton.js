import React from "react";
import '../css/Button.css'

class PercentButton extends React.Component{
    constructor() {
        super();
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn"  id="functional">
                <p>%</p>
            </div>
        )
    }

    inputClick() {
        let result = Number(this.props.state.result);
        let temp = this.props.state.temp;
        switch (this.props.state.operator){
            case "plus":
                result = temp / 100 * result;
                temp = temp + result;
                console.log(temp)
                break;
            case "minus":
                result = temp / 100 * result;
                temp = temp - result;
                break;
            case "multiply":
                result = result / 100;
                temp = temp * result;
                break;
            case "divide":
                result = result / 100;
                temp = temp / result;
                break;
            default:
                this.props.inputClick({
                    result: "0",
                    temp: 0,
                    operator: "equal",
                    digitAppend: false,
                })
                return;
        }

        this.props.inputClick({
            result: String(result),
            temp: temp,
            digitAppend: false,
            operator: "percent",
        })
    }
}

export default PercentButton