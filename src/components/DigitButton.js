import React from "react";
import '../css/Button.css'

class DigitButton extends React.Component{
    constructor() {
        super();
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn">
                <p>{this.props.digit}</p>
            </div>
        )
    }

    inputClick(){
        let result;
        let digitAppend = true

        if (this.props.state.digitAppend == false) {
            result = this.props.digit;
            if (this.props.digit == "0"){
                digitAppend = false
            }
        }
        else{
            result = this.props.state.result + this.props.digit
        }

        this.props.inputClick({
            result: result,
            digitAppend: digitAppend,
        })
    }
}

export default DigitButton