import React from "react";
import '../css/Button.css';
import {ReactComponent as SvgTitle} from '../images/PlusMinus.svg';

class PlusMinusButton extends React.Component{
    constructor() {
        super();
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn" id="functional">
                <SvgTitle />
            </div>
        )
    }

    inputClick() {
        this.props.inputClick({
            result: String(Number(this.props.state.result) * -1),
            digitAppend: true,
        })
    }
}

export default PlusMinusButton