import React from "react";
import '../css/Button.css';
import {ReactComponent as SvgTitle} from '../images/Delete.svg';

class DeleteButton extends React.Component{
    constructor() {
        super();
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        return (
            <div onClick={this.inputClick} className="Btn">
                <SvgTitle />
            </div>
        )
    }

    inputClick() {
        let current = this.props.state.result;
        let result = current.substring(0, current.length-1)

        let digitAppend = true;
        if (result.length == 0){
            result = "0";
            digitAppend = false;
        }

        this.props.inputClick({
            result: result,
            digitAppend: digitAppend,
        })
    }
}

export default DeleteButton