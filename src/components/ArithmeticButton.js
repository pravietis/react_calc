import React from "react";
import '../css/Button.css'

class ArithmeticButton extends React.Component{
    constructor(props) {
        super(props);
        this.operator = this.props.operator
        this.inputClick=this.inputClick.bind(this)
    }

    render(){
        const buttonTitle = {
            plus: "+",
            minus: "-",
            multiply: "×",
            divide: "÷",
            equal: "="
        }
        return (
            <div onClick={this.inputClick} className="Btn" id="arithmetical">
                <p>{buttonTitle[this.operator]}</p>
            </div>
        )
    }

    inputClick(){
        if ((this.props.state.digitAppend == false) && (["plus", "minus", "multiply", "divide"].indexOf(this.props.state.operator) != "-1")){
            console.log(this.props.state.operator)
            return
        }

        let result = Number(this.props.state.result);
        let temp = this.props.state.temp;

        switch (this.props.state.operator){
            case "plus":
                result = temp + result;
                break;
            case "minus":
                result = temp - result;
                break;
            case "multiply":
                result = temp * result;
                break;
            case "divide":
                result = temp / result;
                break;
            case "equal":
                break;
            case "percent":
                result = temp;
                console.log(temp);
                break;
            default:
                this.props.inputClick({
                    result: "0",
                    temp: 0,
                    operator: "equal",
                    digitAppend: false,
                });
                return;
        }

        this.props.inputClick({
            result: String(result),
            temp: result,
            operator: this.operator,
            digitAppend: false,
        });
    }
}

export default ArithmeticButton