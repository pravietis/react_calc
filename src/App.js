import React from "react";
import './css/App.css'

import ResultDisplay from "./components/ResultDisplay";
import DigitButton from "./components/DigitButton";
import ClearButton from "./components/ClearButton";
import PercentButton from "./components/PercentButton";
import PlusMinusButton from "./components/PlusMinusButton";
import DeleteButton from "./components/DeleteButton";
import DotButton from "./components/DotButton";
import ArithmeticButton from "./components/ArithmeticButton";


class App extends React.Component {
    constructor() {
        super();
        this.state = {
            result: "0",
            temp: 0,
            operator: "equal",
            digitAppend: false,
        }
    }

    render() {
        return (
            <div className="App">
                <ResultDisplay result={this.state.result}/>
                <div className="Buttons">
                    <ClearButton inputClick={(st) => this.setState(st)}/>
                    <PlusMinusButton state={this.state} inputClick={(st) => this.setState(st)}/>
                    <PercentButton state={this.state} inputClick={(st) => this.setState(st)}/>
                    <ArithmeticButton operator="divide" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="7" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="8" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="9" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <ArithmeticButton operator="multiply" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="4" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="5" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="6" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <ArithmeticButton operator="minus" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="1" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="2" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="3" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <ArithmeticButton operator="plus" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DotButton state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DigitButton digit="0" state={this.state} inputClick={(st) => this.setState(st)}/>
                    <DeleteButton state={this.state} inputClick={(st) => this.setState(st)}/>
                    <ArithmeticButton operator="equal" state={this.state} inputClick={(st) => this.setState(st)}/>
                </div>
            </div>
        )
    };
}

export default App;
